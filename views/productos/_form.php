<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NomProducto')->textInput(['maxlength' => true]) ?>

    <?php  // echo $form->field($model, 'IdGrupo')->dropDownList($model->listarGrupos())
    
//            echo $form->field($model, 'IdGrupo')->
//                    widget(\yii\jui\AutoComplete::className(),[
//                        'options' => ['class' => 'form-control'],
//                        'clientOptions' => [
//                             'source' =>$model->gruposAutocompletar(),
//                        ],
//                    ]);
    ?>
    
    <?php
        // Combina un dropdownlist con autocompletar. Necesitamos libreria de kartik
        echo $form->field($model, 'IdGrupo')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->listarGrupos(),
                    'options' => ['placeholder' => 'Selecciona un grupo'],
                    'pluginOptions' => [
                    'allowClear' => true
        ],
    ]);
    ?>
     

    <?= $form->field($model, 'Precio')->input("number") ?>

    <?= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
