<?php

use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=  Html::a('<i class="fad fa-th-large"></i> Tarjeta', ["index"],['class'=>'btn btn-primary text-white m-3']) ?>
        <?= Html::a('<i class="fas fa-user-plus"></i> Nuevo producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdProducto',
            'NomProducto',
            'IdGrupo',
            [
                "label" => "Nombre del grupo",
                "attribute" => 'idGrupo.NombreGrupo', // nombre del getter para Grupos del modelo Productos + .campo que quieras mostrar
            ],
            //Precio,
            //'precioFinal',
            // Si usamos precioFinal no podemos ordenar los productos por precios
            //hacemos lo siguiente para que nos deje ordenar por precio en el GridView
            [
              "attribute" => "Precio",
              "value" => function ($model){
                    return $model->precioFinal;
              }
            ],
            [
                "attribute" => "foto",
                "format"=> "raw",
                "value" => function ($model){
                    if (isset($model->foto) && $model->foto!=""){
                        return Html::img("@web/imgs/$model->IdProducto/$model->foto",
                            ["class" => "img-thumbnail"],
                            
                        );
                    }
                    $nombre="anonimo.png";
                    return Html::img("@web/imgs/$nombre",
                        ["class" => "img-thumbnail"],
                        );
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Productos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdProducto' => $model->IdProducto]);
                 },
                "options" => [
                    "class" => "col-2"
                ]
            ],
                    
            
        ],
    ]); ?>


</div>
