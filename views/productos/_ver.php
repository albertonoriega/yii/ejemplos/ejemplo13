<div class="row">
    <div class="col-lg-12">
        <h3>Id: <?= $model->IdProducto ?><br></h3>
        <div class="text-dark bg-info rounded p-2">Nombre producto </div>
        <div class="p-1"><?= $model->NomProducto ?></div>
        <div class="text-dark bg-info rounded p-2">Precio </div>
        <div class="p-1"><?= $model->precioFinal ?></div>
        <div class="text-dark bg-info rounded p-2">Id Grupo: </div>
        <div class="p-1"><?= $model->IdGrupo ?></div>
        <div class="text-dark bg-info rounded p-2">Nombre del grupo: </div>
        <div class="p-1"><?= $model->idGrupo->NombreGrupo ?></div>
        <div class="text-dark bg-info rounded p-2">Foto: </div>
        <div class="p-1">
        <?php  
            if(isset($model->foto) && $model->foto!=""){ // comprobamos que existe la foto y que sea distinta de vacio
                echo yii\helpers\Html::img(
                        "@web/imgs/$model->IdProducto/$model->foto",
                        ["class" => "img-thumbnail"],
                        ); // como hemos guardado la foto de los esparragos en una carpeta con su ID ponemos $model->IdProducto
                           // el nombre de la foto lo hemos metido en la tabla de la BBDD por lo que esta guardado en $model->foto 
                   
            } else {
                echo yii\helpers\Html::img(
                        "@web/imgs/anonimo.png",
                        ["class" => "img-thumbnail"],
                        );
                
            }
        ?>
        </div>
        <div class="p-1 mb-5">
            <?php
                //BOTON DE VER
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
                        ['productos/view', 'IdProducto'=> $model->IdProducto], //controlador/acción y parametro
                        ['class'=> 'btn btn-primary mr-2']); //estilos del botón   
                
                // BOTON DE ACTUALIZAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-pen-square"></i>',
                        ['productos/update', 'IdProducto'=> $model->IdProducto],
                        ['class'=> 'btn btn-success mr-2']);
                
                // BOTON DE ELIMINAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-trash"></i>',
                        ['productos/delete', 'IdProducto' => $model->IdProducto],
                        [
                            'class' => 'btn btn-danger mr-2',
                            'data' => [
                                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                                'method' => 'post',
                        ], // esto solo para boton borrar
                ])
            ?>
        </div>
        <br class="float-none">
    </div>
</div>