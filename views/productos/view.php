<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = "Id: " .$model->IdProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'IdProducto' => $model->IdProducto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'IdProducto' => $model->IdProducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estás seguro que quieres borrar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdProducto',
            'NomProducto',
            'IdGrupo',
            
            // Esto solo sirve con relaciones 1 a infinito
            //[
            //    "label" => "Nombre del grupo",
            //    "attribute" => 'idGrupo.NombreGrupo', // nombre del getter para Grupos del modelo Productos + .campo que quieras mostrar
            // ],
            
            // Esto sirve para todos los casos
            [
                'label' => 'Nombre del grupo',
                'value' => function ($model){
                    return $model->idGrupo->NombreGrupo;
                }
            ],
            'precioFinal',
            [
                "attribute" => "foto",
                "format"=> "raw",
                "value" => function ($model){
                    if (isset($model->foto) && $model->foto!=""){
                        return Html::img("@web/imgs/$model->IdProducto/$model->foto",
                            ["class" => "img-thumbnail col-6 d-block m-auto"],
                            
                        );
                    }
                    $nombre="anonimo.png";
                    return Html::img("@web/imgs/$nombre",
                        ["class" => "img-thumbnail col-6 d-block m-auto"],
                        );
                }
            ],
        ],
    ]) ?>

    <!-- GRID VIEW QUE TE MUESTRA TODAS LAS VENTAS DEL PRODUCTO -->
    <br>
    <h3 align="center">VENTAS DEL PRODUCTO</h3>
    <br>
    <?php yii\widgets\Pjax::begin() // Te permite moverte por las ventas sin que vuelva a cargar los datos del producto?> 
    <?= yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idventas',
            'CodVendedor',
            'CodProducto',
            //'FechaFinal',
            [
              "attribute" => "Fecha",
              "value" => function ($model){
                    return $model->FechaFinal;
              }
            ],
            [
              "attribute" => "Kilos",
              "value" => function ($model){
                    return $model->kilosFinal;
              }
            ],
//            [
//                'class' => ActionColumn::className(),
//                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
//                    return Url::toRoute([$action, 'idventas' => $model->idventas]);
//                 }
//            ],
        ],
    ]); ?>
    <?php yii\widgets\Pjax::end() ?>
    
</div>
