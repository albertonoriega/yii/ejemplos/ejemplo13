<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = 'Actualizar producto: ' . $model->IdProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Id: " . $model->IdProducto, 'url' => ['view', 'IdProducto' => $model->IdProducto]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
