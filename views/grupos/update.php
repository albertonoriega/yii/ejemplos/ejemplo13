<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Grupos $model */

$this->title = 'Actualizar grupo Id: ' . $model->IdGrupo;
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Id: " .$model->IdGrupo, 'url' => ['view', 'IdGrupo' => $model->IdGrupo]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="grupos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
