<?php

use app\models\Ventas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-index">

    
    
    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>
        <?=  Html::a('<i class="fad fa-th-large"></i> Tarjeta', ["index"],['class'=>'btn btn-primary text-white m-3']) ?>
        <?= Html::a('<i class="fas fa-user-plus"></i> Nueva venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

       <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idventas',
            'CodVendedor',
            'CodProducto',
            //'FechaFinal',
            [
              "attribute" => "Fecha",
              "value" => function ($model){
                    return $model->FechaFinal;
              }
            ],
            [
              "attribute" => "Kilos",
              "value" => function ($model){
                    return $model->kilosFinal;
              }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idventas' => $model->idventas]);
                 }
            ],
        ],
    ]); ?>


</div>
