<div class="row">
    <div class="col-lg-12">

        <h4>Id: <?= $model->idventas ?><br></h4>
        
        <?php
        $items = [

            [
                'header' => 'Vendedor',
                'content' => $model->ListaVendedores // Usamos el Getter que hemos generado en el modelo de ventas
            ],
            [
                'header' => 'Producto',
                'content' => $model->ListaProductos
            ],
        ];
                
                
                
           

        echo \yii\jui\Accordion::widget([
            'items' =>
            $items,
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div'],
            'headerOptions' => ['tag' => 'h3'],

            'clientOptions' => ['collapsible' => true, 'active' => false],
        ]);
        ?>

        
        
        
        
        <div class="text-dark bg-info rounded p-2">Fecha: </div>
        <div class="p-1"><?= $model->FechaFinal ?></div>
        <div class="text-dark bg-info rounded p-2">Kilos: </div>
        <div class="p-1"><?= $model->KilosFinal ?></div>
        <div class="p-1 mb-5">


<?php
//BOTON DE VER
echo \yii\helpers\Html::a(
        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
        ['ventas/view', 'idventas' => $model->idventas], //controlador/acción y parametro
        ['class' => 'btn btn-primary mr-2']); //estilos del botón   
// BOTON DE ACTUALIZAR
echo \yii\helpers\Html::a(
        '<i class="fas fa-2x fa-pen-square"></i>',
        ['ventas/update', 'idventas' => $model->idventas],
        ['class' => 'btn btn-success mr-2']);

// BOTON DE ELIMINAR
echo \yii\helpers\Html::a(
        '<i class="fas fa-2x fa-trash"></i>',
        ['ventas/delete', 'idventas' => $model->idventas],
        [
            'class' => 'btn btn-danger mr-2',
            'data' => [
                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                'method' => 'post',
            ], // esto solo para boton borrar
        ])
?>
        </div>
        <br class="float-none">
    </div>
</div>
