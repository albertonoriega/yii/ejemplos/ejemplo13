<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */

$this->title = "Id: " . $model->idventas;
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ventas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idventas' => $model->idventas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idventas' => $model->idventas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres borrar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idventas',
            //'CodVendedor',
            
            // Usando el getter que trae el modelo por defecto ( te saca todos los cambios)
//            [
//                'attribute' => 'CodVendedor',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::ul($model->codVendedor);
//                }
//            ],
//            
              // Usando el getter que hemos creado nosotros en el modelo para vendedores
            [
                'attribute' => 'ListaVendedores',
                'format' => 'raw',
            ],
            //'CodProducto',
            // Usando el getter que hemos creado nosotros en el modelo para productos
            [
                'attribute' => 'ListaProductos',
                'format' => 'raw',
            ],
            [
              "attribute" => "Fecha",
              "value" => function ($model){
                    return $model->FechaFinal;
              }
            ],
            'Kilos',
        ],
    ]) ?>

</div>
