<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ventas-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?php
        // Combina un dropdownlist con autocompletar. Necesitamos libreria de kartik
        echo $form->field($model, 'CodVendedor')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->listarVendedores(),
                    'options' => ['placeholder' => 'Selecciona un vendedor'],
                    'pluginOptions' => [
                    'allowClear' => true
        ],
    ]);
    ?>
    
    <?php
        // Combina un dropdownlist con autocompletar. Necesitamos libreria de kartik
        echo $form->field($model, 'CodProducto')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->listarProductos(),
                    'options' => ['placeholder' => 'Selecciona un producto'],
                    'pluginOptions' => [
                    'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'Fecha')->input("datetime-local") ?>

    <?= $form->field($model, 'Kilos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
