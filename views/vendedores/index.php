<?php

use app\models\Vendedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=  Html::a('<i class="fal fa-table"></i> Tabla', ["indexg"],['class'=>'btn btn-primary text-white m-3']) ?>
        <?= Html::a('<i class="fas fa-user-plus"></i> Nuevo vendedor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => "_ver", // vista que se carga por cada registro
        "itemOptions" => [  // itemOptions es para cada elemento
                   'class' => 'col-lg-4 ml-auto mr-auto bg-light p-3 mb-5',
                ],
        "options" => [  // options es para todo el componente
                'class' => 'row', // hacemos que todas las cajitas estén en la misma fila
                ],
        'layout' => " {items} {pager}",
    ]) ?>


</div>
