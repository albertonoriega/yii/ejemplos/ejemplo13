<?php

use app\models\Vendedores;
use app\models\Ventas;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/** @var View $this */
/** @var Vendedores $model */

$this->title = "Id: " . $model->IdVendedor;
$this->params['breadcrumbs'][] = ['label' => 'Vendedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="vendedores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'IdVendedor' => $model->IdVendedor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'IdVendedor' => $model->IdVendedor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres borrar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdVendedor',
            'NombreVendedor',
            [
                'attribute' => 'FechaAlta',
                // En detail view no funciona content, hay que poner value
                'value' => function($model){
                    return $model->FechaAltaFinal;
                }
            ],
            'NIF',
            [
                'attribute' => 'FechaNac',
                'value' => function($model){
                    return $model->FechaNacFinal;
                }
            ],
            'Direccion',
            'Poblacion',
            'CodPostal',
            'Telefon',
            'EstalCivil',
            'activo:boolean',
        ],
    ]) ?>
    
    <!-- GRID VIEW QUE TE MUESTRA TODAS LAS VENTAS DEL VENDEDOR -->
    <br>
    <h3 align="center">VENTAS DEL VENDEDOR</h3>
    <br>
        
    <?php yii\widgets\Pjax::begin() // Te permite moverte por las ventas sin que vuelva a cargar los datos del vendedor?> 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'idventas',
            'CodProducto',
            //'FechaFinal',
            [
              "attribute" => "Fecha",
              "value" => function ($model){
                    return $model->FechaFinal;
              }
            ],
            [
              "attribute" => "Kilos",
              "value" => function ($model){
                    return $model->kilosFinal;
              }
            ],
//            [
//                'class' => ActionColumn::className(),
//                'urlCreator' => function ($action, Ventas $model, $key, $index, $column) {
//                    return Url::toRoute(['ventas/'.$action, 'idventas' => $model->idventas]);
//                 }
//            ],
        ],
    ]); ?>
    <?php yii\widgets\Pjax::end() ?>

</div>
