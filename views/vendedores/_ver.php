<div class="row">
    <div class="col-lg-12">
        <h3>Id: <?= $model->IdVendedor ?><br></h3>
        <div class="text-white bg-secondary rounded p-2">Nombre vendedor </div>
        <div class="p-1"><?= $model->NombreVendedor ?></div>
        <div class="text-white bg-secondary rounded p-2">Fecha de alta </div>
        <div class="p-1"><?= $model->FechaAltaFinal ?></div>
        <div class="text-white bg-secondary rounded p-2">NIF </div>
        <div class="p-1"><?= $model->NIF ?></div>
        <div class="text-white bg-secondary rounded p-2">Fecha de nacimiento </div>
        <div class="p-1"><?= $model->FechaNacFinal ?></div>
        <div class="text-white bg-secondary rounded p-2">Más información </div>
        <?php
        echo \yii\jui\Accordion::widget([
            'items' => [
                [
                    'header' => 'Dirección',
                    'content' => $model->Direccion,
                ],
                [
                    'header' => 'Población',
                    'content' => $model->Poblacion,
                ],
                [
                    'header' => 'Telefono',
                    'content' => $model->Telefon,
                ],
                [
                    'header' => 'Estado civil ',
                    'content' => $model->EstalCivil,
                ],
                [
                    'header' => 'Activo ',
                    'content' => $model->activo,
                ],
            ],
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div'],
            'headerOptions' => ['tag' => 'h3'],
            'clientOptions' => ['collapsible' => true,'active'=>false],
        ]);
        ?>
        <div class="p-1 mb-5">
            <?php
            //BOTON DE VER
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
                        ['vendedores/view', 'IdVendedor'=> $model->IdVendedor], //controlador/acción y parametro
                        ['class'=> 'btn btn-primary mr-2']); //estilos del botón   
                
                // BOTON DE ACTUALIZAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-pen-square"></i>',
                        ['vendedores/update', 'IdVendedor'=> $model->IdVendedor],
                        ['class'=> 'btn btn-success mr-2']);
                
                // BOTON DE ELIMINAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-trash"></i>',
                        ['vendedores/delete', 'IdVendedor' => $model->IdVendedor],
                        [
                            'class' => 'btn btn-danger mr-2',
                            'data' => [
                                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                                'method' => 'post',
                        ], // esto solo para boton borrar
                ])
            ?>
        </div>
        <br class="float-none">
    </div>
</div>

