<?php

use app\models\Vendedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=  Html::a('<i class="fad fa-th-large"></i> Tarjeta', ["index"],['class'=>'btn btn-primary text-white m-3']) ?>
        <?= Html::a('<i class="fas fa-user-plus"></i> Nuevo vendedor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdVendedor',
            'NombreVendedor',
            //'FechaAlta',
            //'FechaAltaFinal', // no se puede ordenar por fecha
            // Para ordenar por fecha hacemos lo siguiente
            [
                'attribute' => 'FechaAlta',
                'content' => function($model){
                    return $model->FechaAltaFinal;
                }
            ],
            'NIF',
            //'FechaNac',
            [
                'attribute' => 'FechaNac',
                'content' => function($model){
                    return $model->FechaNacFinal;
                }
            ],
            
            //'Direccion',
            //'Poblacion',
            //'CodPostal',
            //'Telefon',
            //'EstalCivil',
            //'activo:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Vendedores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdVendedor' => $model->IdVendedor]);
                 }
            ],
        ],
    ]); ?>


</div>
