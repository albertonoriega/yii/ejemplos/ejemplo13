<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedores".
 *
 * @property int $IdVendedor
 * @property string|null $NombreVendedor
 * @property string|null $FechaAlta
 * @property string|null $NIF
 * @property string|null $FechaNac
 * @property string|null $Direccion
 * @property string|null $Poblacion
 * @property string|null $CodPostal
 * @property string|null $Telefon
 * @property string|null $EstalCivil
 * @property bool|null $activo
 *
 * @property Ventas[] $ventas
 */
class Vendedores extends \yii\db\ActiveRecord
{
    
    private string $fechaAltaFinal; // propiedad del getter que vamos a crear debajo
    private string $fechaNacFinal;
    private string $activoFinal;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FechaAlta', 'FechaNac'], 'safe'],
            [['activo'], 'boolean'],
            [['NombreVendedor', 'Direccion', 'Poblacion', 'Telefon'], 'string', 'max' => 50],
            [['NIF'], 'string', 'max' => 9],
            [['CodPostal'], 'string', 'max' => 5],
            [['EstalCivil'], 'string', 'max' => 15],
            [['NIF'],'unique','message' => 'El {attribute} ya está siendo utilizado'], //Solo puede haber un DNI, no puede haber repetidos
            // {attribute} es un template que sustituye por el nombre de la rule
            [['NIF'], 'required'], // el campo DNI no puede estar vacio
            [['NIF'],'condicion'],
        ]; 
    }
    
    // Metodo que compueba que la letra del DNI metido sea la adecuada
    public function condicion($atributo) { // $atributo = NIF
        if($this->comprobarLetraDni()==false){
            return $this->addError($atributo,'La letra no es correcta para ese DNI');
        }
    }
    
    // Metodo para comprobar la letra del DNI
    private function comprobarLetraDni() {
        $letras=["T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E","T"];
        
        $dni= substr($this->NIF, 0, 8);
        $letra= strtoupper (substr($this->NIF, -1, 1));
        if ($dni < 0 || $dni > 99999999) {
            $salida = false;
        } else {
            $resto = $dni%23;
            $letraCalculada = $letras[$resto];
            if($letraCalculada==$letra) {
                $salida= true;
            } else {
                $salida=false;
            }
            return $salida;
        }
        // Otra forma 
//        $dnientero=$this->NIF;
//        $dni= str_split($dnientero,8);
//        $letradni=$dni[0]%23;
//        $letra=$letras[$letradni];
//        
//        if ($letra==$dni[1]){
//            return true;
//        }
//        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdVendedor' => 'Id Vendedor',
            'NombreVendedor' => 'Nombre del vendedor',
            'FechaAlta' => 'Fecha de alta',
            'NIF' => 'NIF',
            'FechaNac' => 'Fecha de nacimiento',
            'Direccion' => 'Dirección',
            'Poblacion' => 'Población',
            'CodPostal' => 'Código postal',
            'Telefon' => 'Teléfono',
            'EstalCivil' => 'Estado civil',
            'activo' => 'Activo',
        ];
    }
 
     /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['CodVendedor' => 'IdVendedor']);
    }
    
    // Metodo para cambiar la fecha de orden, pasar de fecha inglesa a fecha española
      
    public function getFechaAltaFinal() {

        if (!empty($this->FechaAlta)) {
            return Yii::$app->formatter->asDate($this->FechaAlta, 'php:d/m/Y');
        }
    }
    
    public function getFechaNacFinal(): string {
        if (!empty($this->FechaNac)) {
            return Yii::$app->formatter->asDate($this->FechaNac, 'php:d/m/Y');
        }
    }

     public function getActivoFinal(): string {
        if($this->activo==true){
            return $this->activoFinal = "Si";
        }
        return $this->activoFinal = "No";
    }

}
