<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $idventas
 * @property int $CodVendedor
 * @property int $CodProducto
 * @property string $Fecha
 * @property float|null $Kilos
 *
 * @property Productos $codProducto
 * @property Vendedores $codVendedor
 */
class Ventas extends \yii\db\ActiveRecord
{
    private string $fechaFinal;
    private string $kilosFinal;
    private string $listaVendedores;
    private string $listaProductos;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CodVendedor', 'CodProducto', 'Fecha'], 'required'],
            [['CodVendedor', 'CodProducto'], 'integer'],
            [['Fecha'], 'safe'],
            [['Kilos'], 'number'],
            [['CodProducto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['CodProducto' => 'IdProducto']],
            [['CodVendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedores::class, 'targetAttribute' => ['CodVendedor' => 'IdVendedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idventas' => 'Idventas',
            'CodVendedor' => 'Cod Vendedor',
            'CodProducto' => 'Cod Producto',
            'Fecha' => 'Fecha',
            'Kilos' => 'Kilos',
        ];
    }

    /**
     * Gets query for [[CodProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProducto()
    {
        return $this->hasOne(Productos::class, ['IdProducto' => 'CodProducto']);
    }

    /**
     * Gets query for [[CodVendedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodVendedor()
    {
        return $this->hasOne(Vendedores::class, ['IdVendedor' => 'CodVendedor']);
    }
    
    public function listarVendedores(): array {
        
        $vendedores = Vendedores::find()->all();
        $arrayVendedores = \yii\helpers\ArrayHelper::map(
                $vendedores,
                "IdVendedor",
                "NombreVendedor"
                );
        
        return $arrayVendedores;
    }
    public function listarProductos(): array {
        
        $productos = Productos::find()->all();
        $arrayProductos = \yii\helpers\ArrayHelper::map(
                $productos,
                "IdProducto",
                "NomProducto"
                );
        
        return $arrayProductos;
    }
    
    public function getKilosFinal(): string {
        return $this->kilosFinal  = $this->Kilos . "kg";
    }


    public function getFechaFinal() {

        if (!empty($this->Fecha)) {
            return Yii::$app->formatter->asDate($this->Fecha, 'php:d/m/Y');
        }
    }
    
    // Getter que me devuelva del vendedor los campos que yo quiero
    
    public function getListaVendedores () {
        
        $salida= "Código : " . $this->CodVendedor 
                . "<br><br>Nombre: " . $this->codVendedor->NombreVendedor
                . "<br><br> NIF: " . $this->codVendedor->NIF
                . "<br><br> Fecha de alta: " . $this->codVendedor->FechaAltaFinal
                . "<br><br> Fecha de nacimiento: " . $this->codVendedor->FechaNacFinal
                . "<br><br>"
                // Botón que te enlaza con la información del vendedor
                . \yii\helpers\Html::a(
                        'Más información',
                        ['vendedores/view', 'IdVendedor' => $this->CodVendedor],
                        ['class' => 'btn btn-info']
                );
        
        return $salida;
    }
    
    public function getListaProductos () {
        
        $salida= "Código : " . $this->CodProducto 
                . "<br><br>Nombre: " . $this->codProducto->NomProducto
                . "<br><br> Precio: " . $this->codProducto->PrecioFinal
                . "<br><br>"
                // Botón que te enlaza con la información del producto
                . \yii\helpers\Html::a(
                        'Más información',
                        ['productos/view', 'IdProducto' => $this->CodProducto],
                        ['class' => 'btn btn-info']
                );
        
        return $salida;
    }
}
