<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IdProducto
 * @property string|null $NomProducto
 * @property int|null $IdGrupo
 * @property float|null $Precio
 * @property string|null $foto
 *
 * @property Grupos $idGrupo
 * @property Ventas[] $ventas
 */
class Productos extends \yii\db\ActiveRecord
{
    
    private string $precioFinal;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdGrupo'], 'integer'],
            [['Precio'], 'number'],
            [['NomProducto'], 'string', 'max' => 50],
            [['foto'], 'string', 'max' => 100],
            [['IdGrupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::class, 'targetAttribute' => ['IdGrupo' => 'IdGrupo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Id Producto',
            'NomProducto' => 'Nombre del producto',
            'IdGrupo' => 'Id Grupo',
            'Precio' => 'Precio',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[IdGrupo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdGrupo()
    {
        return $this->hasOne(Grupos::class, ['IdGrupo' => 'IdGrupo']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['CodProducto' => 'IdProducto']);
    }
    
     
    
    public function getPrecioFinal(): string {
        return $this->precioFinal = $this->Precio . "€";
    }

    
    // Listar todos los grupos para hacer un desplegable
    public function listarGrupos(): array {
        
        $grupos = Grupos::find()->all();
        $arrayGrupos = \yii\helpers\ArrayHelper::map(
                $grupos,
                "IdGrupo", // campo que graba
                "NombreGrupo" // campo que muestra
                );
        
        return $arrayGrupos;
    }
    
    public function gruposAutocompletar() {
        
        return Grupos::find()->asArray()
                ->select("IdGrupo as value, NombreGrupo as label")
                ->all();
        // TE crea lo siguiente
        //[
        //    value:1,
        //    label:frutas
        //],
        //[value:2,
        //    label:verduras
        //]
    }
}
